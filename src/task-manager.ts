import path = require("path")

import * as vscode from "vscode"

import { getCblorbPath, getExternalPath, getInform6Path, getInternalPath, getNiPath } from "./inform-paths"
import { InformProjectManager } from "./inform-project"
import { ProcessManager } from "./process-manager"
import { NiTaskDefinition, isNiTaskDefinition } from "./task-providers/ni-task-provider"
import { Inform6nTaskDefinition, Inform6nTaskProvider, isInform6nTaskDefinition } from "./task-providers/inform6n-task-provider"
import { CblorbTaskDefinition, CblorbTaskProvider } from "./task-providers/cblorb-task-provider"


export class TaskManager {
	private processManager: ProcessManager
	private projectManager: InformProjectManager
	private disposables: vscode.Disposable[] = []

	constructor(processManager: ProcessManager, projectManager: InformProjectManager) {
		this.processManager = processManager
		this.projectManager = projectManager

		// Execute Inform 6 when ni finishes.
		vscode.tasks.onDidEndTaskProcess(async (event) => {
			const niDefinition = event.execution.task.definition
			if (!isNiTaskDefinition(niDefinition)) {
				return
			}
			if (!(niDefinition.toInform6 ?? true)) {
				return
			}
			if (event.exitCode) {
				// TODO
				return
			}

			const mode = niDefinition.mode ?? "test"

			const i6nDefinition: Inform6nTaskDefinition = {
				type: Inform6nTaskProvider.Inform6nTaskType,
				project: niDefinition.project,
				debug: mode === "test" || mode === "release-test"
			}

			if (niDefinition.format) {
				i6nDefinition.format = niDefinition.format
			}

			const blorb = niDefinition.blorb ?? await projectManager.getOutputSettingCreateBlorb(niDefinition.project)
			if (mode !== "test" && blorb) {
				i6nDefinition.toCblorb = blorb
			} else {
				i6nDefinition.toCblorb = false
			}

			vscode.tasks.executeTask(await this.getInform6nTask(i6nDefinition))
		}, this, this.disposables)

		// Execute cBlorb when Inform 6 finishes.
		vscode.tasks.onDidEndTaskProcess(async (event) => {
			const i6nDefinition = event.execution.task.definition
			if (!isInform6nTaskDefinition(i6nDefinition)) {
				return
			}
			const toCblorb = i6nDefinition.toCblorb ?? false
			if (!toCblorb) {
				return
			}
			if (event.exitCode) {
				// TODO
				return
			}

			const format = i6nDefinition.format ?? await projectManager.getOutputSettingStoryFormat(i6nDefinition.project)

			vscode.tasks.executeTask(await this.getCblorbTask({
				type: CblorbTaskProvider.CblorbTaskType,
				project: i6nDefinition.project,
				format: format
			}))
		}, this, this.disposables)
	}

	async getNiTask(definition: NiTaskDefinition): Promise<vscode.Task> {
		const informVersion = definition.informVersion ?? await this.projectManager.getOutputSettingCompilerVersion(definition.project)

		const format = definition.format ?? await this.projectManager.getOutputSettingStoryFormat(definition.project)

		const args = [
			"--internal",
			getInternalPath(informVersion),
			"--external",
			getExternalPath(),
			"--project",
			definition.project,
			`--format=${format}`
		]

		const mode = definition.mode ?? "test"

		const noRng = definition.noRng ?? await this.projectManager.getOutputSettingNoRng(definition.project)
		if (mode === "test" && noRng) {
			args.push("--rng")
		}

		if (mode === "release" || mode === "release-test") {
			args.push("--release")
		}

		const niExecution = new NiExecution(
			definition.project,
			getNiPath(informVersion),
			args,
			this.processManager
		)
		const task = new vscode.Task(
			definition,
			vscode.TaskScope.Workspace,
			`compile ${path.basename(definition.project)}`,
			"inform7",
			niExecution.customExecution,
			"$inform7"
		)
		task.presentationOptions = {
			reveal: vscode.TaskRevealKind.Never,
		}
		return task
	}

	async getInform6nTask(definition: Inform6nTaskDefinition): Promise<vscode.Task> {
		const inform6Path = getInform6Path()

		// TODO: -k arg to output I6 debug info.
		const args = [
			"-w", // Ignore warnings,
			"-c", // No code excerpt in error messages.
			"-E1", // Microsoft error style (for the problem matcher).
			"+include_path=../Source,./",
		]

		const format = definition.format ?? await this.projectManager.getOutputSettingStoryFormat(definition.project)
		switch (format) {
			case "z5":
				args.push("-v5")
				break
			case "z8":
				args.push("-v8")
				break
			case "ulx":
				args.push("-G")
				break
		}

		if (definition.debug ?? true) {
			args.push("-SD") // Enable strict and debug.
		} else {
			args.push("-~S~D") // Disable strict and debug.
		}

		/* We use path.join to have absolute paths, so that the problem matcher has absolute paths too. */
		args.push(path.join(definition.project, "Build/auto.inf"))
		args.push(path.join(definition.project, `Build/output.${format}`))

		const task = new vscode.Task(
			definition,
			vscode.TaskScope.Workspace,
			`compile ${path.basename(definition.project)}`,
			"inform7",
			new vscode.ProcessExecution(inform6Path, args, {
				cwd: path.join(definition.project, "Build"),
			}),
			"$inform6n"
		)
		task.presentationOptions = {
			reveal: vscode.TaskRevealKind.Never,
		}
		return task
	}

	async getCblorbTask(definition: CblorbTaskDefinition): Promise<vscode.Task> {
		const cblorbPath = getCblorbPath()

		const args: string[] = []

		switch (process.platform) {
			case "linux":
				args.push("-unix")
				break
			case "win32":
				args.push("-windows")
				break
		}

		const format = definition.format ?? await this.projectManager.getOutputSettingStoryFormat(definition.project)
		let outputExtension: "zblorb" | "gblorb"
		switch (format) {
			case "z5":
			case "z8":
				outputExtension = "zblorb"
				break
			case "ulx":
				outputExtension = "gblorb"
				break
		}

		args.push(
			"Release.blurb",
			`Build/output.${outputExtension}`
		)

		const task = new vscode.Task(
			definition,
			vscode.TaskScope.Workspace,
			`release ${path.basename(definition.project)}`,
			"inform7",
			new vscode.ProcessExecution(cblorbPath, args, {
				cwd: definition.project,
			})
		)
		task.presentationOptions = {
			reveal: vscode.TaskRevealKind.Never,
		}
		return task
	}

	dispose() {
		for (const disposable of this.disposables) {
			disposable.dispose()
		}
	}
}


class NiExecution {
	customExecution: vscode.CustomExecution

	private completion: number
	private writeEmitter: vscode.EventEmitter<string>
	private closeEmitter: vscode.EventEmitter<number>
	private progressEmitter: vscode.EventEmitter<{
		increment: number,
		message: string
	}>
	private disposables: vscode.Disposable[]
	private pseudoTerminal: vscode.Pseudoterminal


	constructor(projectPath: string, niPath: string, args: string[], processManager: ProcessManager) {
		this.completion = 0

		this.disposables = []

		this.writeEmitter = new vscode.EventEmitter<string>()
		this.disposables.push(this.writeEmitter)

		this.closeEmitter = new vscode.EventEmitter<number>()
		this.disposables.push(this.closeEmitter)

		this.progressEmitter = new vscode.EventEmitter<{
			increment: number,
			message: string
		}>()
		this.disposables.push(this.progressEmitter)

		this.pseudoTerminal = {
			open: () => {
				this.writeEmitter.fire(`${niPath} ${args.join(" ")}\r\n`)
				const niProcess = processManager.spawn(niPath, args, {
					"cwd": vscode.workspace.getWorkspaceFolder(vscode.Uri.file(projectPath))?.uri.fsPath
				})

				niProcess.stdout?.on("data", (data) => {
					this.writeEmitter.fire((data as Buffer).toString("latin1"))
				})

				niProcess.stderr?.on("data", (data) => {
					/* Convert the received buffer to a string.
					Since the output can span multiple indented lines, we also collapse new lines followed by a run of spaces into a single space. */
					const output = (data as Buffer).toString("latin1").replace(/(\r\n|\n)\s+/g, " ")

					/* It's a progress message (e.g. "++ 5% (Analysing sentences)").
					We'll fire the progressEmitter to update the progress bar. */
					const progressRe = /\+\+ (\d+)% \(([^)]+)\)/
					let match = progressRe.exec(output)
					if(match) {
						const newCompletion = parseInt(match[1], 10)
						const increment = newCompletion - this.completion
						this.completion = newCompletion
						this.progressEmitter.fire({
							increment,
							message: match[2]
						})
						this.writeEmitter.fire(output)
						return
					}

					/* It's an error message (e.g. "  >--> Your wrote '[bad code]' ([file path], line [N]): [explanation].").
					In that case, we process the message so that it can be parsed by a problem matcher.
					Sometimes a single error points to multiple locations in the source. In that case, only the first location will be caught by the problem matcher. (That's why some * and + are marked as lazy.) */
					const problemRe = /\s+>-->\s+(.*?) \((.+?), line (\d+)\)(.*)/
					match = problemRe.exec(output)
					if (match) {
						/* The message is the 1st and 4th group joined. (The 2nd and 3rd being the file and the line.) */
						const message = `${match[1]}${match[4]}`

						let errorFilePath = match[2]
						/* Ni writes "source text" when the error is in story.ni (rather than an extension), so we replace it with the actual path of the source. */
						if (errorFilePath === "source text") {
							errorFilePath = path.join(projectPath, "Source/story.ni")
						}

						const errorLine = match[3]

						/* Process the message for the problem matcher. It becomes e.g. "  >--> You wrote [bad code]: [explanation]. (Error in [file path], line [N])\r\n" */
						const processedErrorMessage = `  >--> ${message} (Error in ${errorFilePath}, line ${errorLine})\r\n`

						// Write the processed message in the terminal.
						this.writeEmitter.fire(processedErrorMessage)
						return
					}


					this.writeEmitter.fire(output)
				})

				niProcess.on("exit", (code) => {
					this.completion = -1
					this.progressEmitter.fire({
						increment: 0,
						message: "Compiler finished"
					})

					/* code === null means the process terminated because of a signal. We consider it a failure are exit with 1 instead. */
					code = code ?? 1
					if (!code) {
						vscode.window.showInformationMessage("Inform 7 ended with success.")
					} else {
						vscode.window.showErrorMessage("Inform 7 ended in failure.")
					}

					// this.writeEmitter.fire(`Inform 7 has finished with code ${code}.`)
					this.closeEmitter.fire(code)
					this.dispose()
				})

			},
			close: () => {
				// TODO
			},
			onDidWrite: this.writeEmitter.event,
			onDidClose: this.closeEmitter.event,
		}

		this.customExecution = new vscode.CustomExecution(async () => {
			vscode.window.withProgress({
				location: vscode.ProgressLocation.Notification,
				title: "Inform 7",
				cancellable: false
			}, (progress, token) => {
				return new Promise(resolve => {
					this.progressEmitter.event(val => {
						progress.report(val)
						if (this.completion < 0) {
							resolve()
						}
					}, this, this.disposables)
				})
			})

			return this.pseudoTerminal
		})
	}

	dispose() {
		for (const disposable of this.disposables) {
			disposable.dispose()
		}
	}
}
