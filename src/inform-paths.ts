import os = require("os")
import path = require("path")


// TODO: Add default macOS path.

export function getNiPath(version?: string) {
	switch (process.platform) {
		case "darwin":
			return version ?
				"/Applications/Inform.app/Contents/MacOS/ni"
				: `/Applications/Inform.app/Contents/MacOS/${version}/ni`
		case "linux":
			return "/usr/local/libexec/ni"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\ni.exe"
		default:
			return "ni"
	}
}


export function getInform6Path() {
	switch (process.platform) {
		case "darwin":
			return "/Applications/Inform.app/Contents/MacOS/inform6"
		case "linux":
			return "/usr/local/libexec/inform6"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\inform6.exe"
		default:
			return "inform6"
	}
}


export function getCblorbPath() {
	switch (process.platform) {
		case "darwin":
			return "/Applications/Inform.app/Contents/MacOS/cBlorb"
		case "linux":
			return "/usr/local/libexec/cBlorb"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\cBlorb.exe"
		default:
			return "cBlorb"
	}
}


export function getInternalPath(version?: string) {
	switch (process.platform) {
		case "darwin":
			return `/Applications/Inform.app/Contents/Resources/retrospective/${version || "6M62"}`
		case "linux":
			return "/usr/local/share/inform7/Internal"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Internal"
		default:
			return ""
	}
}


export function getExternalPath() {
	switch(process.platform) {
		case "darwin":
			return path.join(os.homedir(), "/Library/Inform")
		case "linux":
			return path.join(os.homedir(), "Inform")
		case "win32":
			return path.join(os.userInfo().homedir, "Documents\\Inform")
		default:
			return path.join(os.homedir(), "Inform")
	}
}
