import * as vscode from "vscode"

import { CommandManager } from "./command-manager"
import { ProcessManager } from "./process-manager"
import { InformProjectManager } from "./inform-project"
import { TaskManager } from "./task-manager"
import { NiTaskProvider } from "./task-providers/ni-task-provider"
import { Inform6nTaskProvider } from "./task-providers/inform6n-task-provider"
import { CblorbTaskProvider } from "./task-providers/cblorb-task-provider"


export function activate(context: vscode.ExtensionContext): void {
	const processManager = new ProcessManager()
	context.subscriptions.push(processManager)

	const informProjectManager = new InformProjectManager()
	context.subscriptions.push(informProjectManager)

	const taskManager = new TaskManager(processManager, informProjectManager)
	context.subscriptions.push(taskManager)

	const niTaskProvider = vscode.tasks.registerTaskProvider(
		NiTaskProvider.NiTaskType,
		new NiTaskProvider(taskManager)
	)
	context.subscriptions.push(niTaskProvider)

	const inform6nTaskProvider = vscode.tasks.registerTaskProvider(
		Inform6nTaskProvider.Inform6nTaskType,
		new Inform6nTaskProvider(taskManager)
	)
	context.subscriptions.push(inform6nTaskProvider)

	const cblorbTaskProvider = vscode.tasks.registerTaskProvider(
		CblorbTaskProvider.CblorbTaskType,
		new CblorbTaskProvider(taskManager)
	)
	context.subscriptions.push(cblorbTaskProvider)

	const commandManager = new CommandManager(taskManager)
	context.subscriptions.push(commandManager)
}
