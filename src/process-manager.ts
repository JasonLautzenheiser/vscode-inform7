import { ChildProcess, execSync, spawn, SpawnOptions } from "child_process"


class DisposableProcess {
	cp: ChildProcess
	ended: boolean

	constructor(cmd: string, args: string[], options: SpawnOptions) {
		this.ended = false

		this.cp = spawn(
			cmd,
			args,
			options
		)
		this.cp.unref()
		this.cp.on("exit", (code) => {
			this.ended = true
		})
	}

	dispose() {
		if (!this.ended && this.cp && !this.cp.killed) {
			if (process.platform == "win32") {
				execSync(`taskkill /pid ${this.cp.pid} /T /F`)
			} else {
				this.cp.kill()
			}
		}
	}
}


/**
 * Class that makes easier to manage child processes and to dispose of them.
 */
export class ProcessManager {
	static MAX_RUNNING = 5
	processes: DisposableProcess[]

	constructor() {
		this.processes = []
	}

	/**
	 * Dispose of all the processes started by this manager (i.e. force-terminate them).
	 */
	dispose() {
		for (const p of this.processes) {
			p.dispose()
			this.cleanup()
			if (this.processes.length) {
				// TODO: Handle this case.
				console.log(`ProcessManager: ${this.processes.length} processes could not be disposed.`)
			}
		}
	}

	/**
	 * Remove the processes that have ended from `this.processes` to free some memory.
	 */
	cleanup() {
		const stillRunning: DisposableProcess[] = []
		for (const p of this.processes) {
			if (!(p.cp && p.cp.killed || p.ended)) {
				stillRunning.push(p)
			}
		}
		if (stillRunning.length > ProcessManager.MAX_RUNNING) {
			// TODO: Handle this case.
			console.log("ProcessManager: Too many processes running!")
		}
		this.processes = stillRunning
	}

	/**
	 * Spawn a new process. The `ChildProcess` created is returned so that events can be attached to it.
	 * @param cmd - The command to run.
	 * @param args - List of string arguments.
	 * @param options - Options to pass to the underlying `child_process.spawn`.
	 */
	spawn(cmd: string, args: string[], options: SpawnOptions) {
		const newProcess = new DisposableProcess(cmd, args, options)
		this.processes.push(newProcess)
		if (this.processes.length > ProcessManager.MAX_RUNNING) {
			this.cleanup()
		}
		return newProcess.cp
	}
}
