import * as vscode from "vscode"

import { TaskManager } from "../task-manager"


export interface NiTaskDefinition extends vscode.TaskDefinition {
	/**
	 * The type of a ni task is always `"ni"`
	 */
	type: "ni",

	/**
	 * The path to the `.inform` project to compile.
	 */
	project: string,

	/**
	 * The story format in which the Inform project will be compiled.
	 */
	format?: "z5" | "z8" | "ulx",

	/**
	 * The Inform version with which the project will be compiled.
	 */
	informVersion?: "6L02" | "6L38" | "6M62",

	/**
	 * The modes in which the story will be compiled.
	 */
	mode?: "test" | "release" | "release-test",

	/**
	 * Whether or not to bind up the story into a Blorb file upon release (`mode` set to `"release"` or `"release-test"`).
	 */
	blorb?: boolean,

	/**
	 * Whether or not to use a fixed random seed in the compiled story. Defaults to
	 * `false`.
	 */
	noRng?: boolean,

	/**
	 * Whether or not to compile the generated .inf source with Inform 6 when ni has
	 * finished.
	 */
	toInform6?: boolean
}


/* This function has to be updated when the definition above is changed!!! */
export function isNiTaskDefinition(definition: any): definition is NiTaskDefinition {
	// Check if it's a ni task.
	if (definition.type !== NiTaskProvider.NiTaskType) {
		return false
	}

	// Check if the mandatory project is present.
	if (typeof definition.project !== "string") {
		return false
	}

	// Check the format.
	const format = definition.format ?? "ulx"
	if (format !== "z5" && format !== "z8" && format !== "ulx") {
		return false
	}

	// Check Inform version.
	const version = definition.informVersion ?? "6M62"
	if (version !== "6L02" && version !== "6L38" && version !== "6M62") {
		return false
	}

	// Check the mode.
	const mode = definition.mode ?? "test"
	if (mode !== "test" && mode !== "release" && mode !== "release-test") {
		return false
	}

	// Check blorb.
	if (typeof (definition.blorb ?? true) !== "boolean") {
		return false
	}

	// Check noRng.
	if (typeof (definition.noRng ?? false) !== "boolean") {
		return false
	}

	// Check toInform6.
	if (typeof (definition.toInform6 ?? true) !== "boolean") {
		return false
	}

	return true
}


export class NiTaskProvider implements vscode.TaskProvider {
	static readonly NiTaskType = "ni"
	private taskManager: TaskManager

	constructor(taskManager: TaskManager) {
		this.taskManager = taskManager
	}

	async provideTasks(): Promise<vscode.Task[]> {
		// No tasks are auto-detected for the moment.
		// TODO: Maybe scan all Inform files in the workspace and create tasks for them?
		return []
	}

	async resolveTask(_task: vscode.Task): Promise<vscode.Task | undefined> {
		if (isNiTaskDefinition(_task.definition)) {
			return await this.taskManager.getNiTask(_task.definition)
		}
		return undefined
	}
}
