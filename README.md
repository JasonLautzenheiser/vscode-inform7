# Inform 7 extension for VS Code

This extension adds support for [Inform 7](http://inform7.com/) in VS Code.

## Features

The extension adds syntax highlighting to Inform 7 source (`.ni` file extension) and extensions (`.i7x` file extension).

It is recommended to also download the [Inform 6 extension](https://marketplace.visualstudio.com/items?itemName=natrium729.inform-6), so that Inform 6 inclusions in Inform 7 source are coloured.

Additionally, texts used in Vorple's `execute JavaScript command` phrase will have JavaScript highlighting. In other situations, if we want to specify that a text contains a JavaScript command, we can precede it by the comment `[js]`:

```
The JS instruction is a text that varies.
The JS instruction is [js]"window.alert('Hello!')".
```

This is useful for texts that varies or properties.

The extension also colour Preform (`.preform` file extension), but this is likely only of interest to those writing language extensions for Inform 7. Since the syntax for including Inform 6 and Preform in Inform 7 is the same (`(-` and `-)`), we should, similarly than for JS commands, add the comment `[preform]` right before the opening parenthesis, so that Preform is highlighted correctly.

Not doing this may cause the colouring of the inclusion to leak outside of the parentheses. (This may be a temporary solution, if something better is found in the future.)
