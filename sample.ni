"An Inform 7 sample" by Natrium729

Volume 1 - Here's a heading

[This is a comment
with [nested [comments]]
and all is right.]

When play begins:
	say "Some text, with a substitution: [time of day].".
	say "A text with malformed substitutions:] [time [of day] [s".

[A table]
Table 1 - Number in words
1	"one"
2	"two"
3	"three"

[Below, some Inform 6.]
Include (-
! Including some Inform 7 within Inform 6.
Constant YOURSELF = (+ yourself +);

! A routine.
[ Hello x;
	print "Hello ", x;
];
-)

[A phrase invoking Inform 6, with an argument.]
To Hello (T - a thing):
	(- Hello({T}); -)

[Executing a JavaScript command with Vorple.]
Instead of jumping:
	execute JavaScript command "window.alert('You, [the player], jump.');"

[Storing a JavaScript command in a text variable.
For this, we should write the comment "[js]" right before the text.]
The JS instruction is initially [js]"window.alert('Done!')".

[A Preform Inclusion. The comment "[preform]" is necessary.]
Include [preform](-
language French

<indefinite-article> ::=
	/b/ un |
	/c/ une |
	/e/ des
	/z/ <testing-against-a-nonterminal>
-) in the Preform grammar.

Volume 2 - Testing the headings

Book - Here's a book

Part - And now a part

Chapter - Followed by a chapter

Section - Finally a section
